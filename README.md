README
------
This is the repository for MWE discovery. 

DATA
-------
 * [CONLL](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-1989) Word embeddings of dimension 100 precomputed using lowercased automatically annotated raw data in French.
 * [Parseme](https://parsemefr.lis-lab.fr/doku.php?id=start) PARSing and Multi-word Expressions
 * [grammar lexicon]()- grammar lexicon of french MWEs


INSTALLATION
-------

 * Install CONLL WordEmbedding 
       * `curl --remote-name-all https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-1989{/word-embeddings-conll17.tar}`
 * Install CONLL Automatically Annotated Raw Texts in French 
       * `curl --remote-name-all https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-1989{/French-annotated-conll17.tar})`
 * CoNLL-U Parser
 * CoNLL-U Parser parses a CoNLL-U formatted string into a nested python dictionary. CoNLL-U is often the output of natural language processing tasks.


        pip install conllu
        Or, if you are using conda:
        
        conda install -c conda-forge conllu



CONTACT
------
hadjmed.dhouha@gmail.com


