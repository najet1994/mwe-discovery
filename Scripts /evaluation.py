
import pandas as pd
from sklearn.model_selection import cross_validate
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import precision_score, accuracy_score, make_scorer, recall_score, cohen_kappa_score, f1_score
from sklearn.model_selection import StratifiedKFold




data = pd.read_excel('eval.xlsx', index_col=0)
data = data.dropna()

tree = DecisionTreeClassifier(max_depth=1,)
cross_validate(tree, X, y, cv=3 
               , scoring = {'precision': make_scorer(precision_score, pos_label='oui')
                            ,'recall': make_scorer(recall, pos_label='oui')
                            ,'accuracy': make_scorer(accuracy_score)
                            , 'F1 ': make_scorer(f1_score,pos_label='oui')})



