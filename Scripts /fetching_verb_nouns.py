#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import numpy as np
import re
import csv
import time
import os
import sys

keys = ['Id', 'Form', 'Lemma', 'UPosTag', 'XPosTagA', 'Feats', 'Head', 'DepRel', 'Deps', 'Misc']
prefix_len = len('# text = ')
directory = 'French'

def read_conllu(path, start = 0, end = -1):
    if end > 0 and start - end >= 0 : return
    file = open(path, encoding='utf-8')
    sentences = []
    all_rows = []
    i = 0
    keep = True
    for line in file:
        s_rows = []
        if i < start:
            for l in file:
                if l == '\n' or l == '\n\r':
                    break
        else:
            if i >= end : break
            row = []
            sentences.append(line[prefix_len:])
            for l in file:
                if l == '\n' or l == '\n\r' :break
                cells = l[:-1].split('\t')
                row = [i] + [v for v in cells]
                if row[4] == 'NOUN' or row[4] == 'VERB':
                    if re.match(r'\W', row[3], flags = re.UNICODE) :
                        keep = False
                s_rows.append(row)
        if keep: all_rows += s_rows
        else :
            sentences = sentences[:-1]
            keep = True
        i+=1
    frame = pd.DataFrame(data = all_rows)
    frame.columns = ['SId'] + keys
    frame.set_index(['SId', 'Id'], inplace = True)
    return frame, sentences

def list_verb_noun():
    print('searching verb and noun')
    start = time.time()
    verb_set = set()
    noun_set = set()
    
    for path in os.listdir(directory):
        batch_size = 100000
        batchs = 0
        sentences = []
        while len(sentences) == batch_size or batchs == 0:
            print(batchs)
            data, sentences = read_conllu(directory+'/'+path,  batchs * batch_size, (batchs+1) * batch_size) #get a Dataframe representing the corpus
            batchs +=1
            verbs = data.loc[data.UPosTag == 'VERB']['Form'] #select the line with verbs
            nouns = data.loc[data.UPosTag == 'NOUN']['Form'] #select the line with nouns
            for c in verbs:
                verb_set.add(c)
            for c in nouns:
                noun_set.add(c)
    print('found all verbs and nouns in ', time.time()-start)
    return verb_set, noun_set

def a(verbs, nouns):
    start = time.time()
    print('loading vectors')
    vec = pd.read_csv('fr3.vectors', sep=' ', quoting = csv.QUOTE_NONE, header= None, index_col=0, skiprows = 1,
                usecols = range(101),encoding='utf-8')
    print('vectors loaded in', time.time() - start)

    start = time.time()
    print('fetching verb vector')
    verbs = pd.DataFrame(data=verbs).set_index(0)
    pd.merge(vec, verbs, left_index= True, right_index= True).to_csv('verbs_vec.vectors', sep=' ', header= None)
    print('verbs vectors fetched in', time.time() - start)

    start= time.time()
    print('fetching noun vector')
    nouns = pd.DataFrame(data=nouns).set_index(0)
    pd.merge(vec, nouns, left_index= True, right_index= True).to_csv('nouns_vec.vectors', sep=' ', header= None)
    print('nouns vectors fetched in', time.time() - start)
a(*list_verb_noun())





# In[5]:





# In[6]:





# In[ ]:




