import glob 
import os.path 
from conllu import parse_incr
import nltk 
from nltk.collocations import * 
import re
from pandas import DataFrame
import math
from pandas import ExcelWriter
accepted_list=[]
rejected_list=[]


file = open("faire-historique", "r",encoding="UTF-8") # Argh j'ai tout écrasé !0
liste=[]
for line in file:
    linevector = line.split("\n")
    liste.append(linevector)
#print(liste)
listeclean=[]

for i in range(len(liste)):
    element=liste[i]
    elt=liste[i][0]
    listeclean.append(elt)


def listdirectory(path,list_exp):
    list_exp = [exp.split(' ') for exp in list_exp]
    
    list_count_verb = [0]*len(list_exp)
    list_count_noun = [0]*len(list_exp)
    list_count_exp = [0]*len(list_exp)
    l = glob.glob(path+'/*')
    
    for i in l: 
        if os.path.isdir(i): listdirectory(i)
        else:
            print("ListDirectory() ===> Working with file [" + i + "]")
            if os.path.splitext(i)[1] == '.conllu':
                
                
                data_file = open(i, 'r',encoding='UTF-8') 
                
                for phrase in parse_incr(data_file):
                        deprel=""
                        phraselemma=""
                        upostags=""
                        for token in phrase :
                            phraselemma=phraselemma+" "+ token["lemma"]
                            upostags = upostags+" "+token["upostag"]
                            
                        X=phraselemma.split()
                        Y= upostags.split()
                        for n, (verb, noun) in enumerate(list_exp):
                            exp=verb+" "+noun
                            test_exp_flag = False
                            if verb in X:
                                list_count_verb[n]+=1
                                if noun in X:
                                    list_count_noun[n]+=1
                                    test_exp_flag = True
                            elif noun in X:
                                list_count_noun[n]+=1
                        
                            if test_exp_flag:
                                
                                if exp not in accepted_list:
                                    
                                    accepted_list.append(exp)
                
                                
                     
                    
                data_file.close()

                
                
            else: print ("extension non traite")
    for n, (verb, noun) in enumerate(list_exp):
        exp=verb+" "+noun
        if exp not in accepted_list:
            rejected_list.append(exp)

listdirectory('/users/nhadj/code/extrait_conll',listeclean)


df1 = DataFrame({'Expression_acceptee': accepted_list})
print("first list") 
print(df1)  
    
writer = ExcelWriter('accepted_faire_historique.xlsx')
df1.to_excel(writer, encoding='utf-8', index=False)
writer.save()



