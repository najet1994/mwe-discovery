#s=open("test.txt")
import math
import numpy as np 
import re
from sklearn.metrics.pairwise import cosine_similarity
from nltk.tag.stanford import StanfordPOSTagger as POS_Tag
expression="jeter éponge" 
V_N=expression.split()
Verb=V_N[0]
Noun=V_N[1]
def extractNounOrigin(word):
    wordfile = open("nouns_vec.vectors", "r", encoding="utf8")
    for line in wordfile:
        linevector = line.split(" ")
        if linevector[0]==word:
            return linevector
def extractVerbOrigin(word):
    wordfile = open("verbs_vec.vectors", "r", encoding="utf8")
    for line in wordfile:
        linevector = line.split(" ")
        if linevector[0]==word:
            return linevector

verb_origin=extractVerbOrigin(Verb)
Noun_origin=extractNounOrigin(Noun)
verb_origin.remove(Verb)
lV=list(verb_origin)
V = [float(i) for i in lV]
Noun_origin.remove(Noun)
lN=list(Noun_origin)
N = [float(i) for i in lN]

verbfile = open("verbs_vec.vectors", "r", encoding="utf8")
nounfile = open("verbs_vec.vectors", "r", encoding="utf8")

res= []
for line in verbfile:
    linevector = line.split(" ")
    vector=list(linevector)
    del vector[0]
    U = [float(i) for i in vector]
    similar_Verb = cosine_similarity([U],[V])
    if(similar_Verb>=0.3) and (similar_Verb!=1):
        res.append(line.split(" ")[0]+" "+Noun)
for line in nounfile:
    linevector = line.split(" ")
    vector=list(linevector)
    del vector[0]
    #del vector[-1]
    import numpy as np
    U = [float(i) for i in vector]
    similar_Noun = cosine_similarity([U],[N])
    if(similar_Noun>=0.3)and (similar_Noun!=1):
        res.append(Verb+" "+line.split(" ")[0])
res_clean = []

for line in res:
    res_clean.append(re.sub('1234567890[`~!@#$%^&""*(){[}}|\:;"<,>.?/»”]+', '', line)) 
res_POS=[]
home = 'stanford-postagger-full-2018-10-16/'
_path_to_model = home + '/stanford-postagger/models/french.tagger' 
_path_to_jar = home + '/stanford-postagger/stanford-postagger.jar'
st = POS_Tag(_path_to_model, _path_to_jar) 

for line in res_clean:
    sentence = line
    res_POS.append((st.tag(sentence.split())))
list_pos=[]

for i in range(len(res_POS)):
    element=res_POS[i]
    pos_v=res_POS[i][0]
    pos_n=res_POS[i][1]
    for i,j in pos_v,pos_n:
        if ((pos_v[1]=='VINF') and (pos_n[1]=='NC')):
              list_pos.append((pos_v[0],pos_n[0]))
semi_final_list=[]
semi_final_list=set(list_pos)
liste_filtree=[]
for line in semi_final_list:
    liste_filtree.append(line)
print(liste_filtree)
mon_fichier = open("jeter_eponge.txt", "w",encoding="UTF-8") # Argh j'ai tout écrasé !
for line in liste_filtree:
    candidat=line[0]+" "+line[1]
    print(candidat)
    mon_fichier.writelines(candidat+"\n")
    
    
mon_fichier.close()

