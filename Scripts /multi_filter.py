import glob 
import os.path 
from conllu import parse_incr
import nltk 
from nltk.collocations import * 
import re
from pandas import DataFrame
import math
from pandas import ExcelWriter


def listdirectory(path,list_exp):
    list_exp = [exp.split(' ') for exp in list_exp]
    compteur=0
    list_count_verb = [0]*len(list_exp)
    list_count_noun = [0]*len(list_exp)
    list_count_exp = [0]*len(list_exp)
    list_count_obl = [0]*len(list_exp)
    list_count_obj = [0]*len(list_exp)
    list_count_nsubj = [0]*len(list_exp)
    
    l = glob.glob(path+'/*')
    
    for i in l: 
        if os.path.isdir(i): listdirectory(i)
        else:
            print("ListDirectory() ===> Working with file [" + i + "]")
            if os.path.splitext(i)[1] == '.conllu':
        
                data_file = open(i, 'r',encoding='UTF-8') 
                
                for phrase in parse_incr(data_file):
                        deprels=""
                        phraselemma=""
                        upostags=""
                        ids=""
                        heads=""
                        
                        for token in phrase :
                            phraselemma=phraselemma+" "+ token["lemma"]
                            upostags = upostags+" "+token["upostag"]
                            ids = ids+" "+ str(token["id"])
                            deprels = deprels+" "+str(token["deprel"]).strip()
                            heads = heads+" "+str(token["head"])
                        X=phraselemma.split()
                        Y= upostags.split()
                        d= deprels.split()
                        i= ids.split()
                        h = heads.split()
                        for n, (verb, noun) in enumerate(list_exp):
                            test_exp_flag = False
                            test_obj_obl=False
                            if verb in X:
                                list_count_verb[n]+=1
                                if noun in X:
                                    list_count_noun[n]+=1
                                    test_exp_flag = True
                            elif noun in X:
                                list_count_noun[n]+=1
                            if test_exp_flag:
                             
                                index_noun=X.index(noun)
                                index_verb=X.index(verb)
                             
                                if (abs(index_noun-index_verb)<=3):
                                  
                                    try:     
                                        if(index_noun<index_verb):
                                            
                                            if((index_verb - index_noun) ==1):
                                                list_count_exp[n]+=1
                                            elif ((h[index_verb]==h[index_noun]) and (d[index_verb]=='acl') and(d[index_noun]=='obj')):
                                                list_count_exp[n]+=1
                                                list_count_nsubj[n]+=1
                                            elif ((i[index_noun]==h[index_verb]) and (d[index_noun]=='obj')):
                                                list_count_exp[n]+=1
                                                list_count_obj[n]+=1
                                            elif ((h[index_noun]==i[index_verb]) and (d[index_noun]=='nsubj')):
                                                list_count_exp[n]+=1
                                                list_count_nsubj[n]+=1
                                                
    
                                        else:
                                            if((index_noun-index_verb ) ==1):
                                                list_count_exp[n]+=1
                                            elif ((h[index_noun]==i[index_verb]) and (d[index_noun]=='obl')):
                                                list_count_obl[n]+=1
                                                list_count_exp[n]+=1
                                                
    
                                            elif ((h[index_noun]==i[index_verb]) and (d[index_noun]=='obj')):
                                                list_count_obj[n]+=1
                                                list_count_exp[n]+=1
                                                
                                            else:
                                                testother=True
                                                others =["ADV","ADJ","ADP","DET"]
                                                for item in range( index_verb , index_noun): 
                                                    if(Y[item] not in others ) or( X[item]=='sur') or( X[item]=='dans'):
                                                   
                                                            testaux=False
                                                          
                                                if(testother):
                                                    list_count_exp[n]+=1
                                                    
                                    except IndexError:
                                        print('index error')
                data_file.close()
               
            else: print ("extension non traite")
    liste_verb=[]
    liste_noun=[]
    liste_expression=[]
    liste_pmi=[]
    liste_obl=[]
    liste_obj=[]
    liste_nsubj=[]
    for n in range(len(list_exp)):
        
        
        #print(list_exp[n]," la fréquence est ",list_count_exp[n])
        liste_expression.append(list_count_exp[n])
        
        #print(" verb frequence: ",list_count_verb[n])
        liste_verb.append(list_count_verb[n])
        
        #print("noun frequence: ", list_count_noun[n])
        liste_noun.append(list_count_noun[n])
        liste_obj.append(list_count_obj[n])
        liste_obl.append(list_count_obl[n])
        liste_nsubj.append(list_count_nsubj[n])

        
        if (list_count_exp[n]>0):
            #print("pmi : " , math.log((list_count_exp[n]/(list_count_verb[n]*list_count_noun[n])),10))
            liste_pmi.append(math.log((list_count_exp[n]/(list_count_verb[n]*list_count_noun[n])),10))
        else :
            liste_pmi.append("n/a")
            

    df = DataFrame({'Expression': list_exp, 'Expression_frenquency': liste_expression, 'verb_frequency': liste_verb, 'Noun_frequency': liste_noun,'frequence_obl':liste_obl,'frequence_obj':liste_obj,'frequence_nsubj':liste_nsubj,'PMI':liste_pmi})
    print('                                               ')
    print(df)  
    compteur+=1
    writer = ExcelWriter('candidats_faire_historique{}.xlsx'.format( str(compteur+1)))
    df.to_excel(writer, encoding='utf-8', index=False)
    writer.save()


listdirectory('/users/nhadj/code/conll',exp_to_filter)



