from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer
lemmatizer = FrenchLefffLemmatizer()

import glob 
import os.path 
from conllu import parse_incr

import re
from pandas import DataFrame
import math
from pandas import ExcelWriter
import csv
import pandas as pd
mixte=[]

def read_lexic(path,list_exp):
    lexique = pd.DataFrame([], columns=['<ENT>V', '<ENT>C1'])
    list_exp = [exp.split(' ') for exp in list_exp]
    In=[]
    Out=[]
    list_count_exp = [0]*len(list_exp)
    
    l = glob.glob(path+'\\*')
    
    for i in l: 
        if os.path.isdir(i): listdirectory(i)
        else:
            print("ListDirectory() ===> Working with file [" + i + "]")
            if os.path.splitext(i)[1] == '.csv':
                
                
                df = pd.read_csv(i, sep = ';', encoding='utf-8')
                if '<ENT>V' in df.columns and '<ENT>C1' in df.columns :
                    lexique = lexique.append( df[['<ENT>V', '<ENT>C1']])
                    
                with open(i, newline='',encoding='utf-8' ) as f:
                    reader = csv.reader(f,delimiter=';')

                    for row in reader:
                        ligne=[]
                        for i,value in enumerate (row):
                            row[i] = row[i].strip()
                            ligne.append(lemmatizer.lemmatize(row[i],'v'))
                        
                        
                        for n, (verb, noun) in enumerate(list_exp):
                            
                            
                            exp=verb+' '+noun
                           
                            
                            if (noun in ligne) and ( verb in ligne)   :
                                print(ligne)
                                print("********")
                                if exp not in In:
                                     In.append(exp)
                                
 
           
            else: print ("extension non traite")
    
    
           
    print('IN LEXIC')
    print(In)
    print('Out of lexic')
    print(Out)
    lexique = lexique.dropna()
    lexique.columns = ['verb', 'comp']
    lexique = lexique.loc[(lexique.comp != '<E>') & (lexique.comp != ' <E>') & (lexique.comp != '<E> ')]
    print(lexique)


